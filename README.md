### SAIC Tech Quiz

#### Assignment Description

Build a command line executable using C++ that takes two numbers and either adds, subtracts, multiplies, or divides them based on command line options.

Bonuses:
- Create a GitLab.com public repository and push your code there :white_check_mark:
- Use CMake as the build configuration tool and commit the solution :white_check_mark:
- Confirm that the solution builds on Linux and Windows :white_check_mark:
- Place the math operations code into a library linked to the executable :x:
- Use boost::program_options to handle the command line options :x:


#### Notes
- Setup an EC2 free instance, cloned repo there. Ran CMake and then the subsequent Makefile. Got an executable that ran properly. Inluded `ec2_linux_runsit.png` for "proof".
- Probably could've done the library extraction, but was getting a bit late.
- Wasn't able to get the boost library dependency setup properly within CMake on Windows. Boost:program_options definitely has some nice to haves for making CLI apps. Would've been cleaner to get it setup so I could've used it. Not sure what I was doing wrong here. Went ahead and kept all the options I was trying in the `CMakeLists.txt` file commented out. 