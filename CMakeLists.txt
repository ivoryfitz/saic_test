# Init Setup
cmake_minimum_required(VERSION 3.0)
project(saic_techexam
    VERSION 1.0
    DESCRIPTION "Technical assessment project"
    LANGUAGES CXX)

# Useful hooks if I need platform specific changes...
# if (CMAKE_SYSTEM_NAME STREQUAL "Windows")
#     target_compile_options(${PROJECT_NAME}  PRIVATE /W4)
# elseif (CMAKE_SYSTEM_NAME STREQUAL "Linux")
#     target_compile_options(${PROJECT_NAME}  PRIVATE -Wall -Wextra -Wpedantic)
# elseif (CMAKE_SYSTEM_NAME STREQUAL "Darwin")
#     # other macOS-specific flags for Clang
# endif()

# If time, setup the library stuff. Docs suggest multiple CMakeLists.txt files for this...
# https://cmake.org/examples/
# Something like this though should work...
# add_subdirectory(MyLib)
# add_library(MyLib ${SRC_FILES})
# target_link_libraries(${PROJECT_NAME} MyLib) 

# Boost Library Setup
set (Boost_DEBUG ON)
# find_package(Boost 1.80.0 REQUIRED COMPONENTS program_options)
# if(Boost_FOUND)
# include_directories(${Boost_INCLUDE_DIRS})
# target_link_libraries(${PROJECT_NAME} Boost::program_options)
# endif(Boost_FOUND)

# find_package(Boost REQUIRED COMPONENTS program_options)
# include_directories(${Boost_INCLUDE_DIRS})
# link_directories(${Boost_LIBRARY_DIRS})

add_executable(${PROJECT_NAME} 
    src/main.cpp)