#include <iostream>
#include <string>
// #include "boost/program_options.hpp"
using namespace std;

void printUsage();

int main(int argc, char *argv[]) 
{
    if (argc != 4) {
        printUsage();
    }
    else 
    {
        int num1, num2;
        try 
        {
            num1 = stoi(argv[1]);
            num2 = stoi(argv[2]);
        }
        catch (exception const & e) 
        {
            cout << "Ensure that the first two arguments are valid numbers." << endl;
            cout << "Given: {{" << argv[1] << "}} and {{" << argv[2] << "}}" << endl;
        }
        
        string operation = argv[3];
        if(operation.compare("add") == 0)
        {
            cout << "Result: " << num1 + num2 << endl;
        } else if (operation.compare("sub") == 0)
        {
            cout << "Result: " << num1 - num2 << endl;
        } else if (operation.compare("mult") == 0)
        {
            cout << "Result: " << num1 * num2 << endl;
        } else if (operation.compare("div") == 0)
        {
            cout << "Result: " << static_cast<double>(num1) / num2 << endl;
        } else
        {
            cout << "Error: Unrecognized Operation. Please use 'add', 'sub', 'mult', or 'div'" << endl;
        }
    }
}

void printUsage() {
    cout << "Unexpected inputs." << endl;
    cout << "./saic_techexam <number1> <number2> <operation>" << endl;
    cout << "\t<number1>: A valid integer\n\t<number2>: A valid integer\n\t<operation>: {add|sub|mult|div}" << endl;
    cout << "Example: \n" << "\t./saic_techexam 1 4 add" << endl;
}